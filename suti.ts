import { MongoAdapter } from './adapter/mongodb';
import { CanBePromise, JWKS, KoaContextWithOIDC, Provider, ResourceServer } from 'oidc-provider';
import { Express } from 'express';
import { IssuerValidator } from './issuer.validator';
export async function initSuti(app: Express, jwks: JWKS,scopes: string[], issuer?: string) {
  return MongoAdapter.connect().then(() => startUp(app,jwks,scopes, issuer));
}

const random = (length = 8) => {
  return Math.random().toString(16).substring(2, length);
};

async function startUp(app: Express,jwks : JWKS, scopes: string[], issuer: string | null){
  const defaultIssuer = new IssuerValidator().ensure(issuer || process.env.IDENTITY_DOMAIN);
  const claims = {
      email: ["email", "email_verified"],
      phone: ["phone_number", "phone_number_verified"],
      profile: [
        "birthdate",
        "family_name",
        "gender",
        "given_name",
        "locale",
        "middle_name",
        "name",
        "nickname",
        "picture",
        "preferred_username",
        "profile",
        "updated_at",
        "website",
        "zoneinfo",
      ],
  };
  const defaultsScope = scopes && scopes.length ? scopes[0] : null;
  const getResourceServerInfo =  (ctx: KoaContextWithOIDC, resourceIndicator: string, client: any): CanBePromise<ResourceServer> => {
    return {
      scope: defaultsScope,
      audience: defaultIssuer,
      accessTokenTTL: 600, // 5 min
      accessTokenFormat: "jwt",
      jwt: {
        sign: { alg: "RS256" },
      },
    };
  };

  const resourceIndicators = {
    getResourceServerInfo,
  };

  const features = {
      clientCredentials : { enabled : true },
      introspection: { enabled : true},
      devInteractions: { enabled: process.env.NODE_ENV != 'production' },
      encryption: { enabled: true },
      resourceIndicators
  }
  const cookies = {
    keys: ['suti'+random(10), 'suti'+random(10)]
  };  
  
  const oidc = new Provider(defaultIssuer,{
    adapter : MongoAdapter,    
    claims,
    scopes,
    features,
    jwks,
    cookies
  });
  oidc.proxy = true;
  app.set("trust proxy", true);
  app.use("/identity", oidc.callback());
}
