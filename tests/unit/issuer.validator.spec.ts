import { IssuerValidator } from "../../issuer.validator";

describe('IssuerValidor',() => {
    let issuerValidator: IssuerValidator;
    beforeEach(() => {
        issuerValidator = new IssuerValidator();
    });

    describe('ensure', () => {

        it('Should be null when it doesnt have URL', () => {
            expect(issuerValidator.ensure(null)).toBeNull();
            expect(issuerValidator.ensure(undefined)).toBeUndefined();
        });

        it('Should be add https:// when has empty protocol', () => {
            expect(issuerValidator.ensure('hola.com.pe')).toBe('https://hola.com.pe');
        });

        it('Should replace http:// by https://', () => {
            expect(issuerValidator.ensure('http://hola.com.pe')).toBe('https://hola.com.pe');
        });
    });
});