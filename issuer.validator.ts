export class IssuerValidator{
    constructor(){}
    ensure(issuer: string | null): string | null{
        return !issuer ? issuer : issuer.indexOf('http://') < 0 &&  issuer.indexOf('https://') < 0 ? 'https://' + issuer : issuer.replace('http://','https://');
    }
}